# Cairo (Egypt)

## Transport for Cairo

[Project Website](http://transportforcairo.com/)

Digital Cairo License: CC BY-NC 4.0 

### Available Files and Attribution

**The following data files were created as part of the Digital Cairo project:**

GIS & GTFS Transit data for the GCR, produced by the Digital Cairo project.

GIS:

The Stops and Trips data for the GCR in Shapefile format.

GTFS:

20180820_GTFSfullworking_Bus_Metro. is a functioning and complete GTFS feed for the formal and pratransit in the GCR + the Cairo metro, including the new fare structure accurate as of June 2018. 

20180820_GTFS_CTA-Paratransit_working. is a functioning GTFS for the formal and paratransit in the GCR.


The data is published under a Creative Commons CC BY-NC license. For proper attribution, please add the following text "This data was created by Transport for Cairo ’TfC' with DigitalMatatus and Takween for Integrated Community Development, under the Digital Cairo Project supported by ExpoLive 2020."


*21032018_GTFS_working_ResourceCenter* is a sample GTFS of the paratransit network of El Sheikh Zayed City and 6th of October City, both of which are part of the Greater Cairo Region.

*23062018_GTFSmetroworkingWithFares_ResourceCenter.* is a functioning and complete GTFS feed for the Cairo metro including the new fare structure accurate as of June 2018. 

The data is published under a Creative Commons CC BY-NC license. For proper attribution, please add the following text "This data was created by Transport for Cairo ’TfC' with DigitalMatatus and Takween for Integrated Community Development, under the Digital Cairo Project supported by ExpoLive 2020."

-----------------

**The following maps were designed for Mwasalat Misr:**

*mm_2018_map_6_lines_and_3_metro_.Ar_v1.1_cs6 Resource Center.pdf
mm_2018_map_6_lines_and_3_metro_.v1.5_cs6 Resource Center.pdf*

The data is under copyright, and owned by Mwasalat Misr. Transport for Cairo retains the right to re-publish the maps for non-commercial interest. Reuse is subject to approval of Mwasalt Misr. 

-----------------
**The following Research & Policy Papers are available.**

*TfC_TICD_How can Transit Mapping Contribute to achieving AUM (28-11-2017) Resource Center.pdf*

Commissioned by the Friedrich-Ebert-Stiftung. Published under a Creative Commons CC BY-NC license. For proper attribution, please add the following text "TICD, TfC. How can Transit Mapping contribute to achieving Adequate Urban Mobility? The Case of Cairo, Egypt, 2017."

*TfC Presentation (09-05-2018) Resource Center.pdf*

As created by Mohamed Hegazy for TfC. Published under a Creative Commons CC BY-NC license. 

-----------------


![alt text](http://transportforcairo.com/wp-content/uploads/2018/08/Digital-Cairo-banner.jpg "ExpoLive Logo")

